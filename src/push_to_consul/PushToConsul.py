from pprint import pprint

import yaml, os, logging, sys, consul

class PushToConsul:
    def __init__(self):
        ''' Constructor for this class. '''
        try:
            with open("consul-properties.yaml", 'r') as stream:
                self.serverProperties = yaml.load(stream)
                if (('server' not in self.serverProperties) or ('token' not in self.serverProperties) or ('port' not in self.serverProperties)):
                    print("consul-properties.yaml is malformed. Check README")
                    sys.exit(1)
                self.consulToken = self.serverProperties['token']
                self.consulServer = self.serverProperties['server']
                self.consulPort = self.serverProperties['port']
        except FileNotFoundError as e:
            print(e)
            print("Required file consul-properties.yaml not found. Check README")
            sys.exit(1)
        except yaml.YAMLError as e:
            print(e)
            sys.exit(1)
        self.data = []
        self.__prepare()

    def push(self):
        print('Pushing to consul at ' + self.consulServer + ':'+str(self.consulPort))
        for obj in self.data:
            print("data --> "+obj['key'])
        self.__pushToConsul()

    def __prepare(self):
        try:
            with open("consul.yaml", 'r') as stream:
                consulKeyVals = yaml.load(stream)
        except FileNotFoundError as e:
            print(e)
            print("Required file consul-properties.yaml not found. Check README")
            sys.exit(1)
        except yaml.YAMLError as e:
            print(e)
            sys.exit(1)

        for obj in consulKeyVals:
            if('content_from_file' in obj and 'key' in obj):
                try:
                    with open(obj['content_from_file'], 'r') as f:
                        file_content_data = f.read()
                except Exception as e:
                    print(e)
                    sys.exit(1)
                finally:
                    self.data.append({'key': obj['key'], 'value': file_content_data})
            elif('keys_and_values_from_file' in obj):
                try:
                    with open(obj['keys_and_values_from_file'], 'r') as stream:
                        file_keyval_content = yaml.load(stream)
                        for kv in file_keyval_content:
                            if('key' in kv and 'value' in kv):
                                self.data.append({'key': kv['key'], 'value': kv['value']})
                except FileNotFoundError as e:
                    print(e)
                    print("Referenced file " + obj['keys_and_values_from_file'] + " not found")
                    sys.exit(1)
                except yaml.YAMLError as e:
                    print(e)
                    sys.exit(1)

    def __pushToConsul(self):
        cc = consul.Consul(host=self.consulServer, port=self.consulPort, token=self.consulToken)
        for obj in self.data:
            cc.kv.put(obj['key'], obj['value'])