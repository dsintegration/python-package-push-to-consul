# python-pip-consul-config-pusher

## Description
This is a tool for pushing contents of a git repository to a Consul kv store.

This should be used in conjunction with git hooks, making it possible to update consul kv store when doing "git push"

## Install
Installing via git (if used in other projects, this is the prefered way):
```
pip3 install git+ssh://git@bitbucket.org/mofr93/python-pip-consul-config-pusher.git
```

Install via checkedout repository:
```
pip3 install .
```

This also installs an executable for the program (~/.local/bin/push_to_consul). Only tested on Linux, 
so not sure if it's in the same location for mac. This might not be in your PATH by default.



## Usage
In order for the program to run you will need 2 files in the root of your project folder.

### consul.yaml

A list of files and/or files with a list of keys and values.

```
# cat consul.yaml
---
-
      key: example/tests/example
      content_from_file: example/tests/example.txt
-
      key_and_values_from_file: example/tests/keysandvalues.yaml
```


***content_from_file***

Push raw content of file "example/tests/example.txt" to consul key "example/tests/example".
```
# cat example/tests/example.txt
this is just an example
```

***keys_and_values_from_file***
    
Iterate over keys and values in this file. 
```
# cat example/tests/keysandvalues.yaml
---
-
      key: example/tests/keyvals/testkey1
      value: testval1
-
      key: example/tests/keyvals/testkey2
      value: testval2
-
      key: example/tests/keyvals/testkey3
      value: testval3
```

### consul-properties.yaml

Specifies consul server connection information and ACL token.
This file should be listed in .gitignore if it contains a restricted token. 
```
cat consul-properties.yaml
---
server: tools.dsservice.eu
port: 8500
token: xxxxxxxxxxxxxxxxxxxxxxxxxx
```

**Run program**

To run the program, use the provided executable.
```
# cd repositoryroot/
~/.local/bin/push_to_consul
```