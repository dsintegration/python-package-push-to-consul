from setuptools import setup, find_packages

setup(name='push_to_consul',
      version='0.6',
      description='Package for easily pushing key/val pairs to a consul server',
      author='Morten Frydensberg',
      author_email='mofr@danskespil.dk',
      license='MIT',
      package_dir={'':'src'},
      packages=find_packages(where='src'),
      entry_points={
            'console_scripts': ['push_to_consul=push_to_consul.command_line:main'],
      },
      install_requires=[
            "python-consul",
      ],
      zip_safe=False,
      test_suite='nose.collector',
      tests_require=['nose'])